const webpackConfig = (config) => {
  config.module.rules.push({
    test: /\.svg$/,
    use: ['@svgr/webpack'],
  });

  return config;
};

const nextConfig = {
  webpack: webpackConfig,
  reactStrictMode: true,
};

/** @type {import('next').NextConfig} */
module.exports = nextConfig;
