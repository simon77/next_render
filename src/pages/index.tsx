import Head from 'next/head';
import React, { ChangeEvent, useEffect, useRef, useState } from 'react';
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';

import Layout from '@/components/Layout';
import { SpringPhysics } from '@/utils/springs';

interface SpanElement extends HTMLSpanElement {
  physics: SpringPhysics;
  nextElementSibling: SpanElement | null;
  previousElementSibling: SpanElement | null;
}

const Char = ({ char }: { char: string }) => {
  const spanRef = useRef<SpanElement>(null);
  useEffect(() => {
    if (!spanRef.current || spanRef.current.physics) {
      return;
    }

    const span = spanRef.current;
    span.physics = new SpringPhysics({
      startAt: 48,
      options: { namespace: '--fontsize', friction: 5 },
      update: ({ namespace, value }) => {
        span.style.setProperty(namespace, value.toFixed() + 'px');
      },
    });

    span.addEventListener('mouseover', () => {
      span.physics.to(100);
      span?.nextElementSibling?.physics.to(70);
      span?.previousElementSibling?.physics.to(70);
    });

    span.addEventListener('mouseout', () => {
      span.physics.to(48);
      span?.nextElementSibling?.physics.to(48);
      span?.previousElementSibling?.physics.to(48);
    });
  }, []);

  return <span ref={spanRef}>{char}</span>;
};

export default function Home() {
  const [edit, setEdit] = useState(false);
  const [text, setText] = useState('Simon. Fullstack Javascript Developer');
  const characters = Array.from(text);
  const handleChangeText = (event: ChangeEvent<HTMLInputElement>) => setText(event.target.value);
  const handleBlurText = () => setEdit(false);
  const handleClickText = () => setEdit(true);
  return (
    <>
      <Head>
        <title>Next Render</title>
      </Head>
      <Layout>
        <Container>
          <Grid container alignItems="center" justifyContent="center" sx={{ py: 2 }}>
            <Grid item>
              {edit ? (
                <TextField
                  autoFocus
                  value={text}
                  onChange={handleChangeText}
                  onBlur={handleBlurText}
                  sx={{ minWidth: 300 }}
                />
              ) : (
                <StyledTypography onClick={handleClickText}>
                  {characters.map((char, index) => (
                    <Char char={char} key={index} />
                  ))}
                </StyledTypography>
              )}
            </Grid>
          </Grid>
        </Container>
      </Layout>
    </>
  );
}

const StyledTypography = styled(Typography)(() => ({
  cursor: 'default',
  userSelect: 'none',
  span: {
    fontSize: `var(--fontsize, 48px)`,
    lineHeight: '100px',
  },
}));
