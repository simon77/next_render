import React from 'react';
import { styled } from '@mui/material/styles';

import Navbar from '@/components/Navbar';
import Footer from '@/components/Footer';

const Main = styled('main')(() => ({
  minHeight: 'calc(100vh - 2 * 64px)'
}))

export default function Layout({ children }: any) {
  return (
    <>
      <Navbar />
      <Main>{children}</Main>
      <Footer />
    </>
  );
}
