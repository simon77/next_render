import React, { useMemo } from 'react';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

export default function Footer() {
  const fullYear = useMemo(() => new Date().getFullYear(), []);
  return (
    <Grid container component="footer" alignItems="flex-end" justifyContent="center" sx={{ minHeight: 64 }}>
      <Grid item>
        <Paper square sx={{ p: 1 }}>
          <Typography>Copyright © {fullYear} JS.</Typography>
        </Paper>
      </Grid>
    </Grid>
  );
}
