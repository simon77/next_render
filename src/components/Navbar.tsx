import Link from 'next/link';
import React, { useState } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import SvgIcon from '@mui/material/SvgIcon';
import JavascriptIcon from '@mui/icons-material/Javascript';

interface ComponentProps {
  name?: string;
}

const drawerWidth = 240;
const navItems = [{ title: 'Contact', href: 'https://my.indeed.com/p/simonl-sdnjvdt' }];

function Navbar({ name = 'Next Render' }: ComponentProps) {
  const [mobileOpen, setMobileOpen] = useState(false);
  const handleDrawerToggle = () => setMobileOpen((prevState) => !prevState);
  return (
    <>
      <AppBar component="nav">
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Box component="div" sx={{ display: { xs: 'none', sm: 'block' } }}>
            <Grid container alignItems="center" sx={{ mr: 2, width: 100, height: 64 }}>
              <SvgIcon inheritViewBox component={JavascriptIcon} sx={{ width: 100, height: 64 }} />
            </Grid>
          </Box>
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
          >
            {name}
          </Typography>
          <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
            {navItems.map((item) => (
              <Button component={Link} key={item.title} href={item.href} sx={{ color: '#fff' }}>
                {item.title}
              </Button>
            ))}
          </Box>
        </Toolbar>
      </AppBar>
      <Box component="nav">
        <Drawer
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
              spacing={1}
              sx={{ my: 1 }}
            >
              <Grid item>
                <SvgIcon
                  inheritViewBox
                  component={JavascriptIcon}
                  sx={{ display: 'block', width: 100, height: 32 }}
                />
              </Grid>
              <Grid item>
                <Typography variant="h6">{name}</Typography>
              </Grid>
            </Grid>
            <Divider />
            <List>
              {navItems.map((item) => (
                <ListItem key={item.title} disablePadding>
                  <ListItemButton sx={{ textAlign: 'center' }}>
                    <ListItemText primary={item.title} />
                  </ListItemButton>
                </ListItem>
              ))}
            </List>
          </Box>
        </Drawer>
      </Box>
      <Toolbar />
    </>
  );
}

export default Navbar;
